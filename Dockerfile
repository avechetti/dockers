FROM debian:stable-slim

ENV DEBIAN_FRONTEND=noninteractive

USER root

RUN apt-get update

RUN apt-get install -yq --no-install-recommends \
    apt-utils \
    curl \
    # Install git
    git \
    # Install apache
    apache2 \
    # Install php
    libapache2-mod-php \
    php-cli \
    php-json \
    php-curl \
    php-fpm \
    php-gd \
    php-ldap \
    php-mbstring \
    php-mysql \
    php-pgsql \
    php-soap \
    php-sqlite3 \
    php-xml \
    php-zip \
    php-intl \
    php-imagick \
    # Install tools
    openssl \
    nano \
    graphicsmagick \
    imagemagick \
    ghostscript \
    #mysql-client \
    iputils-ping \
    locales \
    sqlite3 \
    ca-certificates \
    dos2unix \
    #Install Crontab
    cron    

#Actaulizamos todo
RUN apt-get upgrade -y

#Limpiamos     
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN locale-gen en_US.UTF-8 en_GB.UTF-8 de_DE.UTF-8 es_ES.UTF-8 fr_FR.UTF-8 it_IT.UTF-8 km_KH sv_SE.UTF-8 fi_FI.UTF-8

HEALTHCHECK --interval=5s --timeout=3s --retries=3 CMD curl -f http://localhost || exit 1

# Activate Rewrite, Deflate, Expires 
RUN a2enmod rewrite
RUN a2enmod deflate
RUN a2enmod expires

# autorise .htaccess files
RUN sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf

#Timezone - 
RUN echo "America/Argentina/Buenos_Aires" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

# #Copiamos los archivos de inicializacion
#COPY  code /var/www/html/
RUN rm /var/www/html/index.html

RUN echo "<?php phpinfo(); ?>" > /var/www/html/index.php

COPY ./wrapper_script.sh wrapper_script.sh

USER root
RUN dos2unix /wrapper_script.sh

CMD ["/bin/bash","wrapper_script.sh"]
#########################################################################################
